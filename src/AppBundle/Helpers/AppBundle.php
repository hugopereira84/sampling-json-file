<?php

namespace AppBundle\Helpers;

interface SamplerInterface
{
    public function sampleRequests($sampleRequests);
}
