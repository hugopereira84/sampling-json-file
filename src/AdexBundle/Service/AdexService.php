<?php
namespace AdexBundle\Service;

use Symfony\Component\Finder\Finder;
use \AdexBundle\Helpers\SamplerInterface;
use \AdexBundle\Helpers\JsonToArrayMaker;

class AdexService implements SamplerInterface{
    
    /**
     * Selects n elements of array, randomly
     * 
     * @param array $requestsCustomer
     * @param int $numberSelect
     * @return array
     */
    public function random_pick( $requestsCustomer, $numberSelect ) 
    {
        $N = count($requestsCustomer);
        $numberSelect = min($numberSelect, $N);
        $picked = array_fill(0, $numberSelect, 0); 
        $backup = array_fill(0, $numberSelect, 0);
        
        // partially shuffle the array, and generate unbiased selection simultaneously
        // this is a variation on fisher-yates-knuth shuffle
        for ($i=0; $i<$numberSelect; $i++) // O(n) times
        { 
            $selected = mt_rand( 0, --$N ); // unbiased sampling N * N-1 * N-2 * .. * N-n+1
            $value = $requestsCustomer[ $selected ];
            $requestsCustomer[ $selected ] = $requestsCustomer[ $N ];
            $requestsCustomer[ $N ] = $value;
            $backup[ $i ] = $selected;
            $picked[ $i ] = $value;
        }
        
        // restore partially shuffled input array from backup
        // optional step, if needed it can be ignored, e.g $requestsCustomer is passed by value, hence copied
        for ($i=$numberSelect-1; $i>=0; $i--) // O(n) times
        { 
            $selected = $backup[ $i ];
            $value = $requestsCustomer[ $N ];
            $requestsCustomer[ $N ] = $requestsCustomer[ $selected ];
            $requestsCustomer[ $selected ] = $value;
            $N++;
        }
        return $picked;
    }
    
    
    public function sampleRequests($pathToJsonFileou ){
        //VERSION 1, not working, http://stackoverflow.com/questions/26393235/how-to-properly-iterate-through-a-big-json-file
        /*
        $stream = fopen('../'.$pathToJsonFileou, 'r');
        $listener = new \AdexBundle\Helpers\JsonToArrayMaker();
        try {
          $parser = new \JsonStreamingParser\Parser($stream, $listener);
          $parser->parse();
          fclose($stream);
        } catch (Exception $e) {
          fclose($stream);
          throw $e;
        }*/
        
        //VERSION 2
        $info = new \SplFileInfo('../'.$pathToJsonFileou);
        $file = $info->openFile('r');
        foreach ($file as $line_num => $line) {
            $fileContent = json_decode($line, true);
            
            //group number of custmores
            $fullReq = [];
            foreach($fileContent as $req){
                $fullReq[$req['customerID']][] = $req;
            }
            
            //select sample and merge all info
            $sampleData = [];
            foreach($fullReq as $customerId => $req){
                $tempData = $this->random_pick( $req, 5000 );
                
                $sampleData = array_merge($tempData, $sampleData);
            }
            
            
            return $sampleData;
        }
        
    }
}
