<?php

namespace AdexBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use \Symfony\Component\Filesystem\Filesystem;

/**
 * Use: php app/console doctrine:fixtures:load
 */
class LoadUserData implements FixtureInterface
{
   
    
    public function load(ObjectManager $manager)
    {
        
        $listFull = [];
        for($numReq=1; $numReq<55000; $numReq++){
            $customerID = rand(1,4);
            $listFull[] = array(
                'requestID'=>'A',
                'userID' => 1,
                'customerID' => $customerID,
                'userAgent' => 'Firefox', 
                'url' =>"http://theadex.com",
                'timestamp' => time()
            );
            usleep(1000);
        }
        
        $nameFile = 'file_'.time().'.json';
        $fs = new Filesystem();
        try {
            $fs->dumpFile('data/'.$nameFile, json_encode($listFull));
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
        }
    }
}