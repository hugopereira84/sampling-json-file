<?php

namespace AdexBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $adexSample = $this->get('adex.sample');
        $result = $adexSample->sampleRequests('data/file_1474228182.json');
        
        
        return $this->render('AdexBundle:Default:index.html.twig', array('result'=>$result));
    }
}
